const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
var mysql = require('mysql');  
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_nodejs'
});
db.connect((err) =>{
    if(err) throw err;
    console.log('Mysql Connected...');
});

app.get('/gempa_bmkg',(req, res) => {
    let sql = "SELECT * FROM gempa_bmkg";
    let query = db.query(sql, (err, results) => {
        if(err) throw err;
        res.json(results);
    });
});

app.post('/gempa_bmkg',(req, res) => {
    let data = {
        tanggal: req.body.Tanggal,
        jam: req.body.Jam,
        lintang: req.body.Lintang,
        bujur: req.body.Bujur,
        magnitude: req.body.Magnitude,
        kedalaman: req.body.Kedalaman,
        wilayah1: req.body.Wilayah1,
        wilayah2: req.body.Wilayah2,
        wilayah3: req.body.Wilayah3,
        wilayah4: req.body.Wilayah4,
        wilayah5: req.body.Wilayah5,
        potensi: req.body.Potensi
    };
    let sql = "INSERT INTO gempa_bmkg SET ?";
    let query = db.query(sql, data, (err, values) => {
        if(err) throw err;
        console.log("1 record inserted");
        res.json(values);
    });
});

app.listen(5002, function () {
    console.log('Listening on port 5002');
});