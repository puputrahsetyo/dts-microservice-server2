var db = require("./db_config");

db.connect(function(err) {
    if (err) throw err;
    
    let sql = `CREATE TABLE gempa_bmkg 
    (
        id int NOT NULL AUTO_INCREMENT,
        tanggal VARCHAR(255) NOT NULL, 
        jam VARCHAR(255) NOT NULL,
        lintang VARCHAR(20) NOT NULL,
        bujur VARCHAR(20) NOT NULL,
        magnitude VARCHAR(10) NOT NULL,
        kedalaman VARCHAR(10) NOT NULL,
        wilayah1 VARCHAR(100),
        wilayah2 VARCHAR(100),
        wilayah3 VARCHAR(100),
        wilayah4 VARCHAR(100),
        wilayah5 VARCHAR(100),
        potensi VARCHAR(100),
        PRIMARY KEY (id)
    )`;
    db.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Table created");
    });
});